//
//  SettingsViewController.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/5/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppDelegate.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize snagSwitch;
@synthesize cancelButton;
@synthesize userInfoDict;
@synthesize switchStateNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    
    }
    return self;
}

- (void)viewDidLoad
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    userInfoDict = appDelegate.userInformation;
    
    [self.view setBackgroundColor:[UIColor viewFlipsideBackgroundColor]];
    
	UILabel *fbLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 200, 30)];
    fbLabel.textColor = [UIColor whiteColor];
    fbLabel.backgroundColor = [UIColor clearColor];
    fbLabel.text = @"Facebook Connection";
    [self.view addSubview:fbLabel];
    
    UISwitch *fbSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(230, 20, 60, 40)];
    [fbSwitch setOn:YES];
    [fbSwitch addTarget:self action:@selector(logoffFB:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:fbSwitch];
        
    cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelButton.frame = CGRectMake(60, 420, 200, 30);
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(returnToMain:)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelButton];
    
}


-(void)logoffFB:(id)sender{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (FBSession.activeSession.isOpen) {
        [appDelegate closeSession];
    } else {
        [appDelegate openSessionWithAllowLoginUI:YES];
    }

}


-(void)returnToMain:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
