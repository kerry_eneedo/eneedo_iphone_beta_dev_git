//
//  EneedoTabViewController.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/31/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "EneedoTabViewController.h"
#import "SettingsViewController.h"
#import "ScanViewController.h"
#import "ItemsViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"

@interface EneedoTabViewController () <UITableViewDataSource,UINavigationControllerDelegate,CLLocationManagerDelegate,
UIActionSheetDelegate>
@property (strong, nonatomic) FBUserSettingsViewController *settingsViewController;
@property (strong, nonatomic) FBCacheDescriptor *placeCacheDescriptor;
@property (strong, nonatomic) FBProfilePictureView *userProfileImage;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) UILabel   *userNameLabel;

- (void)setPlaceCacheDescriptorForCoordinates:(CLLocationCoordinate2D)coordinates;

@end

@implementation EneedoTabViewController
@synthesize userNameLabel = _userNameLabel;
@synthesize settingsViewController = _settingsViewController;
@synthesize placeCacheDescriptor = _placeCacheDescriptor;
@synthesize userProfileImage = _userProfileImage;
@synthesize locationManager = _locationManager;
@synthesize session;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        UIButton *settingsButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        [settingsButtonView setImage:[UIImage imageNamed:@"settingbutton117x118"] forState:UIControlStateNormal];
        [settingsButtonView addTarget:self action:@selector(configureEneedoSettings:)forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *rBB = [[UIBarButtonItem alloc] initWithCustomView:settingsButtonView];
        self.navigationItem.rightBarButtonItem = rBB;
        
        
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Get the CLLocationManager going.
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    // We don't want to be notified of small changes in location, preferring to use our
    // last cached results, if any.
    self.locationManager.distanceFilter = 50;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sessionStateChanged:)
                                                 name:ENEEDOSessionStateChangedNotification
                                               object:nil];
    
    [FBSession openActiveSessionWithAllowLoginUI:NO];
    
    if(FBSession.activeSession.isOpen){
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                self.userNameLabel.text = user.name;
                  self.userProfileImage.profileID = [user objectForKey:@"id"];
            }
         }];
        
    
    }
    
    UIButton *fbStatusButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [fbStatusButtonView setImage:[UIImage imageNamed:@"settingbutton117x118"] forState:UIControlStateNormal];
    [fbStatusButtonView addTarget:self action:@selector(configureEneedoSettings:)forControlEvents:UIControlEventTouchUpInside];
    
    UIView *fbFrameStatusView = [[UIView alloc] initWithFrame:CGRectMake(2, 2, 180, 44)];
        
    _userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(34, 18, 200, 10)];
    _userNameLabel.font = [UIFont systemFontOfSize:8.0];
    _userNameLabel.textColor = [UIColor whiteColor];
    _userNameLabel.backgroundColor = [UIColor clearColor];
    [fbFrameStatusView addSubview:_userNameLabel];
    
    UIBarButtonItem *lBB = [[UIBarButtonItem alloc] initWithCustomView:fbFrameStatusView];
    self.navigationItem.leftBarButtonItem = lBB;
    
    _userProfileImage = [[FBProfilePictureView alloc] init];
    _userProfileImage.frame = CGRectMake(0, 7, 30, 30);
    [fbFrameStatusView addSubview:_userProfileImage];
    
    UIImageView *fbLogo = [[UIImageView alloc] initWithFrame:CGRectMake(20, 26, 13, 13)];
    fbLogo.image = [UIImage imageNamed:@"f_logo"];
    [fbFrameStatusView addSubview:fbLogo];

    
}


- (void)viewDidUnload {
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void) configureEneedoSettings:(id)sender{
    SettingsViewController *svc = [[SettingsViewController alloc] init];
    [self.navigationController presentViewController:svc animated:YES completion:nil];
}



#pragma mark - FBUserSettingsDelegate methods

- (void)sessionStateChanged:(NSNotification*)notification {
}

- (void)loginViewController:(id)sender receivedError:(NSError *)error{
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error: %@",
                                                                     [AppDelegate FBErrorCodeDescription:error.code]]
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}


#pragma mark -
#pragma mark CLLocationManagerDelegate methods

- (void)startLocationManager {
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    if (!oldLocation ||
        (oldLocation.coordinate.latitude != newLocation.coordinate.latitude &&
         oldLocation.coordinate.longitude != newLocation.coordinate.longitude &&
         newLocation.horizontalAccuracy <= 100.0)) {
            // Fetch data at this new location, and remember the cache descriptor.
            [self setPlaceCacheDescriptorForCoordinates:newLocation.coordinate];
            [self.placeCacheDescriptor prefetchAndCacheForSession:FBSession.activeSession];
        }
}

- (void)setPlaceCacheDescriptorForCoordinates:(CLLocationCoordinate2D)coordinates {
    //    self.placeCacheDescriptor =
    //    [FBPlacePickerViewController cacheDescriptorWithLocationCoordinate:coordinates
    //                                                        radiusInMeters:1000
    //                                                            searchText:@"restaurant"
    //                                                          resultsLimit:50
    //                                                      fieldsForRequest:nil];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
	NSLog(@"%@", error);
}

@end
