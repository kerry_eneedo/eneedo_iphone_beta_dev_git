//
//  AppDelegate.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

extern NSString *const ENEEDOSessionStateChangedNotification;
extern NSString *const SnagTypeChangedNotification;

@class EneedoTabViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>{

    
    UIWindow *window;
    NSMutableDictionary *userInformation;
    NSMutableArray *mobileSnagsInformation;
    NSMutableArray *wheelTitleInformation;
    UIView *loadScreen;
    NSString *deviceTokenString;
    UIImageView *logo;
    NSString *startupWheelTitle;
    NSMutableDictionary *sessionDict;
    
}
@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) NSMutableDictionary *userInformation;
@property (retain, nonatomic) NSString *deviceTokenString;
@property (retain, nonatomic) NSString *startupWheelTitle;
@property (retain, nonatomic) NSMutableArray *mobileSnagsInformation;
@property (retain, nonatomic) NSMutableArray *wheelTitleInformation;
@property (retain, nonatomic) UIView *loadScreen;
@property (retain, nonatomic) UIImageView *logo;
@property (strong, nonatomic) FBSession *session;

@property (retain, nonatomic) NSMutableDictionary *sessionDict;

-(void)queryWheelData:(NSString *)wheelTitle;
-(void)customizeNavBars;

// FBSample logic
// The app delegate is responsible for maintaining the current FBSession. The application requires
// the user to be logged in to Facebook in order to do anything interesting -- if there is no valid
// FBSession, a login screen is displayed.
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
+ (NSString *)FBErrorCodeDescription:(FBErrorCode) code;
- (void) closeSession;


+ (AppDelegate *)sharedAppDelegate;
- (NSURL *)smartURLForString:(NSString *)str;
@end
