//
//  ItemTileView.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "ItemTileView.h"
#include <math.h>


@implementation ItemTileView
@synthesize tileColor;
@synthesize index;
@synthesize username;
@synthesize tilePortrait;
@synthesize tileImageView;
@synthesize itemName;
@synthesize shortDescription;
@synthesize longDescription;
@synthesize tileImageName;
@synthesize image_url;
@synthesize imageData;
@synthesize rateUp;
@synthesize rateDown;
@synthesize snags;
@synthesize reSnags;
@synthesize price;
@synthesize eneedoItemID;
@synthesize itemNameLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [[self layer] setCornerRadius:9.0f];
        [[self layer] setMasksToBounds:YES];
        CGColorRef tc = [UIColor darkGrayColor].CGColor;
        if(!self.tileColor){
            [[self layer] setBackgroundColor:tc];
        }
    }
    return self;
}



- (void)drawRect:(CGRect)rect
{
    if(tilePortrait){
        NSLog(@" Image presence; Size: Width:%f Height:%f",tilePortrait.size.width,tilePortrait.size.height);
        CGSize newSize = CGSizeMake(110, 110);
        UIGraphicsBeginImageContext(newSize);
        [tilePortrait drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *newImage = [[UIImage alloc] init];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CGPoint recordPoint = CGPointMake(-20,-20);
        [newImage drawAtPoint:recordPoint];
    }else{
        NSLog(@"No Image");
    }
    
}

@end