//
//  UIWebViewAdditions.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/27/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "UIWebViewAdditions.h"

@implementation UIWebView(UIWebViewAdditions)

- (CGSize)windowSize {
    CGSize size;
    size.width = [[self stringByEvaluatingJavaScriptFromString:@"window.innerWidth"] integerValue];
    size.height = [[self stringByEvaluatingJavaScriptFromString:@"window.innerHeight"] integerValue];
    return size;
}

- (CGPoint)scrollOffset {
    CGPoint pt;
    pt.x = [[self stringByEvaluatingJavaScriptFromString:@"window.pageXOffset"] integerValue];
    pt.y = [[self stringByEvaluatingJavaScriptFromString:@"window.pageYOffset"] integerValue];
    return pt;
}

@end