//
//  Wheel.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Wheel : NSObject{
    
    NSString        *username;
    //UIImage         *usernamePortrait;

    NSString        *snagAction;
    NSMutableArray  *tilesArray;
    
}
@property (nonatomic, copy) NSString  *username;
//@property (nonatomic, copy) UIImage  *usernamePortrait;
@property (nonatomic, copy) NSString  *snagAction;
@property (nonatomic, copy) NSMutableArray  *tilesArray;

@end