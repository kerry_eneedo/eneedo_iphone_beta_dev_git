//
//  ItemTileView.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ItemTileView : UIView
{
    UIColor *tileColor;
    NSString *username;
    UIImage  *tilePortrait;
    UIImageView   *tileImageView;
    NSString *itemName;
    UILabel  *itemNameLabel;
    NSString *shortDescription;
    NSString *longDescription;
    NSString *tileImageName;
    NSURL    *image_url;
    NSData   *imageData;
    NSNumber *snags;
    NSNumber *reSnags;
    NSNumber *rateUp;
    NSNumber *rateDown;
    NSNumber *price;
    NSNumber *eneedoItemID;///id in database
    NSUInteger     index;
    
}

@property (nonatomic, retain) UIColor *tileColor;
@property (nonatomic, copy) NSString  *username;
@property (nonatomic, copy) UIImage  *tilePortrait;
@property (nonatomic, copy) UIImageView   *tileImageView;
@property (nonatomic, copy) NSString  *itemName;
@property (nonatomic, copy) NSURL    *image_url;
@property (nonatomic, copy) NSData   *imageData;
@property (nonatomic, copy) UILabel  *itemNameLabel;
@property (nonatomic, copy) NSString *shortDescription;
@property (nonatomic, copy) NSString *longDescription;
@property (nonatomic, copy) NSString  *tileImageName;
@property (nonatomic, copy) NSNumber  *rateUp;
@property (nonatomic, copy) NSNumber  *rateDown;
@property (nonatomic, copy) NSNumber  *snags;
@property (nonatomic, copy) NSNumber  *reSnags;
@property (nonatomic, copy) NSNumber  *price;
@property (nonatomic, copy) NSNumber  *eneedoItemID;
@property (assign) NSUInteger index;


@end