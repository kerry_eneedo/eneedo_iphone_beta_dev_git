//
//  SnagItemDetailViewController.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/28/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "SnagItemDetailViewController.h"

@interface SnagItemDetailViewController ()

@end

@implementation SnagItemDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
