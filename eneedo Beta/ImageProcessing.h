//
//  ImageProcessing.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/26/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageProcessing : NSObject
+(UIImage *)checkEneedoImageCriteria:(NSString *)imageURLString;
+(UIImage *)packageSnagImageCrop:(UIImage *)rawSnagImage;
@end
