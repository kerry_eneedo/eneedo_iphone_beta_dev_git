//
//  Product.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize imageData;
@synthesize name;
@synthesize price;
@synthesize shortDescription;
@synthesize longDescription;
@synthesize list;
@synthesize storeId;
@synthesize storeName;
@synthesize storeLocation;
@synthesize imageUrl;


@end
