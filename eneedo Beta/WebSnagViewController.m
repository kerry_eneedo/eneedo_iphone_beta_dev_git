//
//  WebSnagViewController.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/9/12.
//  Copyright (c) 2012 eneedo. All rights reserved.

#import "WebSnagViewController.h"
#import "SnagDetailViewController.h"
#import "AppDelegate.h"
#import "TFHpple.h"
#import "Product.h"
#import "ItemTileView.h"
#import "LinkCompare.h"
#import "StoreSnags.h"
#import "ImageProcessing.h"
#import "UnpreventableUILongPressGestureRecognizer.h"
#import "UIWebViewAdditions.h"
#import "SnagItemDetailViewController.h"


@interface WebSnagViewController ()
- (void)longPressRecognized:(UILongPressGestureRecognizer *)gestureRecognizer;

- (void)goBack:(id)sender;
- (void)goForward:(id)sender;
- (void)reloadOrStop:(id)sender;

@end

@implementation WebSnagViewController
@synthesize mainWebView;
@synthesize snagImageView,pageURLTitle,searchTextField;
@synthesize rootDomainString,itemGenericTitle;
@synthesize actionActionSheet = _actionActionSheet;
@synthesize loadingIndicator;





- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    
    itemGenericTitle = [[NSString alloc] init];
    
    
    NSString   *stringURL = @"http://www.eneedo.com";
    NSURL *url = [NSURL URLWithString:stringURL];
    
        
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
        
    NSError *error;
    NSString *eneedoPage = [NSString stringWithContentsOfURL:url
                                                    encoding:NSASCIIStringEncoding
                                                       error:&error];
    NSLog(@"WebPage: %@",eneedoPage);
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    
    UIMenuItem *imageSnag = [[UIMenuItem alloc] initWithTitle: @"Snag" action: @selector(getSnagImage:)];
    [menuController setMenuItems: [NSArray arrayWithObjects:imageSnag, nil]];
    
    UIBarButtonItem *bbt = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(backToRoot:)];
    self.navigationItem.leftBarButtonItem = bbt;
    
    UIToolbar *browseBar = [[UIToolbar alloc] initWithFrame:CGRectZero];
    [browseBar sizeToFit];
    browseBar.frame = CGRectMake(0,0,self.view.frame.size.width,browseBar.frame.size.height);
    browseBar.barStyle = UIBarStyleBlackTranslucent;
    browseBar.autoresizesSubviews = YES;
        
    UIBarButtonItem *flexspace2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    
    UIBarButtonItem *forwardButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"browserForward.png"] style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(goForward:)];
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"browserBack.png"]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(goBack:)];
     
    NSArray *browseItems = [[NSArray alloc] initWithObjects:backButton,flexspace2,forwardButton, nil];

    browseBar.items = browseItems;
    
    pageURLTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 1.5, 300, 8)];
    pageURLTitle.shadowOffset = CGSizeMake(0,-1);
    pageURLTitle.shadowColor  = [UIColor blackColor];
    pageURLTitle.backgroundColor = [UIColor clearColor];
    pageURLTitle.textColor = [UIColor whiteColor];
    pageURLTitle.textAlignment = NSTextAlignmentCenter;
    pageURLTitle.font = [UIFont fontWithName:@"Helvetica" size:7];
    pageURLTitle.text = stringURL;
    [browseBar addSubview:pageURLTitle];
        
    searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(40, 9, 240, 26)];
    searchTextField.delegate = self;
    searchTextField.borderStyle = UITextBorderStyleRoundedRect;
    searchTextField.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    [browseBar addSubview:searchTextField];
    
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(255, 12.2, 20, 20)];
    loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [loadingIndicator setHidden:NO];
   // loadingIndicator.backgroundColor = [UIColor clearColor];
    [browseBar addSubview:loadingIndicator];
    
    
    [self.view addSubview:browseBar];

    mainWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, browseBar.frame.size.height , self.view.frame.size.width, self.view.frame.size.height - browseBar.frame.size.height)];
    mainWebView.scalesPageToFit = YES;
    mainWebView.delegate = self;
    [mainWebView loadRequest:urlRequest];
    [self.view addSubview:mainWebView];
    
    
    // Create a long press recognizer for handling links long press
    UnpreventableUILongPressGestureRecognizer *longPressRecognizer = [[UnpreventableUILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressRecognized:)];
    longPressRecognizer.allowableMovement = 20;
    longPressRecognizer.minimumPressDuration = 1.0f;
    [mainWebView addGestureRecognizer:longPressRecognizer];
}


#pragma mark UIWebView Delegate methods
- (BOOL)webView:(UIWebView *) sender shouldStartLoadWithRequest:(NSURLRequest *) request navigationType:(UIWebViewNavigationType) navigationType {
    if ([request.URL.absoluteString isEqual:@"about:blank"])
        return NO;
    req = (NSMutableURLRequest *)request;
    
    return YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [loadingIndicator startAnimating];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    // Disable the defaut actionSheet when doing a long press
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none';"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    NSString *currentURL =  webView.request.URL.absoluteString;
    [loadingIndicator stopAnimating];
    ///Adding the title to the snag tile by parsing the webPage
    
    NSData *pageData = [NSData dataWithContentsOfURL:webView.request.URL];
    
    TFHpple *pageParser = [TFHpple hppleWithHTMLData:pageData];
    
    NSString *pageXpathQueryString = @"//title";
    NSArray *pageNodes = [pageParser searchWithXPathQuery:pageXpathQueryString];
    
    itemGenericTitle = nil;
    for (TFHppleElement *element in pageNodes) {
        
        
        if(pageNodes.count == 1){
            NSLog(@"element: %@",element.text);
            itemGenericTitle = element.text;
        }else if(pageNodes.count > 1 || pageNodes.count == 0 ){
            itemGenericTitle = @"My Latest Snag";
        }
    }
    
    pageURLTitle.text = currentURL;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{



}


- (BOOL) canPerformAction:(SEL)action withSender:(id)sender
{
    
      
    return NO;
}
-(void)goBack:(id)sender{    
    [mainWebView goBack];
    NSString *currentURL =  mainWebView.request.URL.absoluteString;
    pageURLTitle.text = currentURL;
    //[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateLocationField) object:nil];
    //[self performSelector:@selector(updateLocationField) withObject:nil afterDelay:1.];
}

-(void)goForward:(id)sender{
   [mainWebView goForward];
    NSString *currentURL =  mainWebView.request.URL.absoluteString;
    pageURLTitle.text = currentURL;
}



- (void)reloadOrStop:(id) sender {
    if (mainWebView.loading)
        [mainWebView stopLoading];
    else [mainWebView reload];
}

#pragma mark -
#pragma mark UILongPressGestureRecognizer handling

- (void)longPressRecognized:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint point = [gestureRecognizer locationInView:mainWebView];
        
        // convert point from view to HTML coordinate system
        CGSize viewSize = [mainWebView frame].size;
        CGSize windowSize = [mainWebView windowSize];
        
        CGFloat f = windowSize.width / viewSize.width;
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 5.) {
            point.x = point.x * f;
            point.y = point.y * f;
        } else {
            // On iOS 4 and previous, document.elementFromPoint is not taking
            // offset into account, we have to handle it
            CGPoint offset = [mainWebView scrollOffset];
            point.x = point.x * f + offset.x;
            point.y = point.y * f + offset.y;
        }
        
        // Load the JavaScript code from the Resources and inject it into the web page
        NSBundle *bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"eneedoBeta" ofType:@"bundle"]];
        
        NSString *path = [bundle pathForResource:@"JSTools" ofType:@"js"];
        NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        [mainWebView stringByEvaluatingJavaScriptFromString: jsCode];
        
        // get the Tags at the touch location
        NSString *tags = [mainWebView stringByEvaluatingJavaScriptFromString:
                          [NSString stringWithFormat:@"MyAppGetHTMLElementsAtPoint(%i,%i);",(NSInteger)point.x,(NSInteger)point.y]];
        
        NSString *tagsHREF = [mainWebView stringByEvaluatingJavaScriptFromString:
                              [NSString stringWithFormat:@"MyAppGetLinkHREFAtPoint(%i,%i);",(NSInteger)point.x,(NSInteger)point.y]];
        
        NSString *tagsSRC = [mainWebView stringByEvaluatingJavaScriptFromString:
                             [NSString stringWithFormat:@"MyAppGetLinkSRCAtPoint(%i,%i);",(NSInteger)point.x,(NSInteger)point.y]];
        NSLog(@"tags : %@",tags);
        NSLog(@"href : %@",tagsHREF);
        NSLog(@"src : %@",tagsSRC);
        
        NSString *url = nil;
        if ([tags rangeOfString:@",IMG,"].location != NSNotFound) {
            url = tagsSRC;
        }
        if ([tags rangeOfString:@",A,"].location != NSNotFound) {
            url = tagsHREF;
        }
        NSLog(@"Long Press url : %@",url);
        
        NSArray *urlArray = [[url lowercaseString] componentsSeparatedByString:@"/"];
        NSString *urlBase = nil;
        if ([urlArray count] > 2) {
            urlBase = [urlArray objectAtIndex:2];
        }
        
        if ((url != nil) &&
            ([url length] != 0)) {
            NSLog(@"Url is working");

            // Save URL for the request
            _urlToHandle = [[NSURL alloc] initWithString:url];
            
            // ask user what to do
            UIAlertView *added = [[UIAlertView alloc] init];
                        
            NSString *snag =[NSString stringWithFormat:@"Your snag: %@",itemGenericTitle];
            [added setTitle:NSLocalizedString(snag,"")];
            [added setDelegate:self];
            [added addButtonWithTitle:NSLocalizedString(@"OK","")];
            [added addButtonWithTitle:NSLocalizedString(@"Cancel","")];
            [added show];
            }else{
            
   

        }
    }
}


#pragma mark -
#pragma mark UIAlertView delegate


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    switch (buttonIndex) {
        case 0:
        {
            //////Add the new item to Snag Wheel
            NSError *err;
            ItemTileView  *newTileView = [[ItemTileView alloc]init];
            NSData *newImageData = [NSData dataWithContentsOfURL:_urlToHandle options:NSDataReadingMappedIfSafe error:&err];
            UIImage *newTileImage = [ImageProcessing packageSnagImageCrop:[UIImage imageWithData:newImageData]];
            newTileView.imageData = UIImagePNGRepresentation(newTileImage);
            newTileView.itemName = itemGenericTitle;
            NSLog(@"URL: %@",_urlToHandle);
            
            
            NSLog(@" Item: %@ image size:%i W:%f0.0 H:%f0.0",newTileView.itemName, newTileView.imageData.length, newTileImage.size.width,newTileImage.size.height);
            
            
            [StoreSnags shortStoreSnag:newTileView.imageData :newTileView.itemName :_urlToHandle];
            
            

        }
            break;
        case 1:
            break;
        default:
            break;
    }

}



#pragma mark Text Methods

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSURL               *url;
    NSString *textSearch = [searchTextField.text stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSString *queryString = [NSString stringWithFormat:@"http://www.google.com/search?q=%@",textSearch];

    url = [NSURL URLWithString:queryString];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    
    [mainWebView loadRequest:urlRequest];

    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backToRoot:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
