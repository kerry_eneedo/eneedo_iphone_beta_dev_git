//
//  ItemsViewController.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "ItemsViewController.h"
#import "ItemTileView.h"
#import "AppDelegate.h"

@interface ItemsViewController ()

@end

@implementation ItemsViewController
@synthesize itemCanvas;
@synthesize itemTitle;
@synthesize itemPrice;
@synthesize snags;
@synthesize reSnags;
@synthesize rateUp;
@synthesize rateDown;
@synthesize itemImageView;
@synthesize commentsTable;
@synthesize tileScroll;
@synthesize tileData;
@synthesize fakeTileCounterNumber;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = NSLocalizedString(@"Items", @"Items");
        self.tabBarItem.image = [UIImage imageNamed:@"tiles_icon"];
    }
    return self;
}



- (void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen
                                                  mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    //[[UIApplication sharedApplication] setStatusBarHidden:NO animated:YES];
    self.view = view;
    
    ///Set tile count later use the array of tiles also loadsup the tiles data
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    tileData = appDelegate.mobileSnagsInformation;
        
    itemCanvas = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 440)];
    itemCanvas.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:itemCanvas];
    
    UIImageView *eneedoBlackGradientBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"eneedeo_gen_overlay_640x960"]];
    eneedoBlackGradientBackground.frame = CGRectMake(0, -30, 320, 480);
    [self.view addSubview:eneedoBlackGradientBackground];
    
    
    itemTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 7, 310, 20)];
    itemTitle.backgroundColor = [UIColor clearColor];
    itemTitle.textColor = [UIColor darkGrayColor];
    itemTitle.font = [UIFont boldSystemFontOfSize:17];
    [self.view addSubview:itemTitle];
    
    itemImageView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 30, 190, 190)];// 216.7, 180
    // itemImageView.contentMode = UIViewContentModeScaleAspectFi;
   // itemImageView.contentMode=UIViewContentModeCenter;
    //itemImageView.contentMode = UIViewContentModeScaleAspectFill;
    //itemImageView.contentMode = UIViewContentModeScaleAspectFill;

    [self.view addSubview:itemImageView];
    
    UIImageView *imageframe = [[UIImageView alloc] initWithFrame:CGRectMake(6, 30, 190, 190)];
    imageframe.image = [UIImage imageNamed:@"itemframe210x210"];
    [self.view addSubview:imageframe];
    
    UIImageView *priceFrame = [[UIImageView alloc] initWithFrame:CGRectMake(219, 32, 100, 39.83)];
    priceFrame.image = [UIImage imageNamed:@"itempricestaticframe123x49"];
    [self.view addSubview:priceFrame];
    
    itemPrice = [[UILabel alloc] initWithFrame:CGRectMake(219, 31, 100, 39.83)];
    itemPrice.backgroundColor = [UIColor clearColor];
    itemPrice.textColor = [UIColor whiteColor];
    itemPrice.textAlignment = NSTextAlignmentCenter;
    itemPrice.shadowOffset = CGSizeMake(-1, 1);
    itemPrice.shadowColor = [UIColor grayColor];
    itemPrice.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:itemPrice];
    
    UILabel *snagsLabel = [[UILabel alloc] initWithFrame:CGRectMake(221, 68, 50, 14)];
    snagsLabel.text = @"Snags";
    snagsLabel.backgroundColor = [UIColor clearColor];
    snagsLabel.font = [UIFont fontWithName:@"Arial-ItalicMT" size:11];
    snagsLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:snagsLabel];
    
    UILabel *reSnagsLabel = [[UILabel alloc] initWithFrame:CGRectMake(221, 82, 50, 14)];
    reSnagsLabel.text = @"Resnags";
    reSnagsLabel.backgroundColor = [UIColor clearColor];
    reSnagsLabel.font = [UIFont fontWithName:@"Arial-ItalicMT" size:11];
    reSnagsLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:reSnagsLabel];
    
    UIImageView *yesArrow = [[UIImageView alloc] initWithFrame:CGRectMake(259, 98, 13, 13)];
    yesArrow.image = [UIImage imageNamed:@"yesbuttonsq47x47"];
    [self.view addSubview:yesArrow];
    
    UIImageView *noArrow = [[UIImageView alloc] initWithFrame:CGRectMake(259, 114, 13, 13)];
    noArrow.image = [UIImage imageNamed:@"nobuttonsq47x47"];
    [self.view addSubview:noArrow];
    
    snags = [[UILabel alloc] initWithFrame:CGRectMake(276, 68, 35, 14)];
    snags.font = [UIFont boldSystemFontOfSize:12];
    [self.view addSubview:snags];
    
    reSnags = [[UILabel alloc] initWithFrame:CGRectMake(276, 82, 35, 14)];
    reSnags.font = [UIFont boldSystemFontOfSize:12];
    [self.view addSubview:reSnags];
    
    rateUp = [[UILabel alloc] initWithFrame:CGRectMake(276, 97.5, 35, 14)];
    rateUp.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:rateUp];
    
    rateDown = [[UILabel alloc] initWithFrame:CGRectMake(276, 114.5, 35, 14)];
    rateDown.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:rateDown];
    
    commentsTable = [[UITableView alloc] initWithFrame:CGRectMake(6, 215, 308, 70)];
    commentsTable.delegate = self;
    commentsTable.dataSource = self;
    commentsTable.rowHeight = 20;
    commentsTable.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    [self.view addSubview:commentsTable];
    
    ///Get the scrollview details and set the frame
    CGRect pagingTileScrollFrame = [self frameForPagingScrollView];
    tileScroll = [[UIScrollView alloc] initWithFrame:pagingTileScrollFrame];
    tileScroll.showsVerticalScrollIndicator = NO;
    tileScroll.contentSize = [self contentSizeForPagingScrollView];
    tileScroll.contentInset = UIEdgeInsetsMake(0, 0, 0, -100.0);
    tileScroll.backgroundColor = [UIColor grayColor];
    tileScroll.delegate = self;
    
    [self.view addSubview:tileScroll];
    [self constructWheel];
}


- (void)constructWheel
{
    for (int i = 0;i < [tileData count]; i++) {
        
                if(i == 0){
                    ItemTileView *realTile = [[ItemTileView alloc] init];
                    realTile.itemName = @"bookend";
                    realTile.tileImageName = @"tiles_icon@2x";
                    realTile.tilePortrait = [UIImage imageNamed:realTile.tileImageName];
                    realTile.frame = CGRectMake((20+100*i), 5, 80, 80);
                    NSLog(@"Center Tile: %i X: %f",i,realTile.center.x);
                    NSLog(@"Center Tile: %i Y: %f",i,realTile.center.y);
                    realTile.index = i;
                    [tileScroll addSubview:realTile];
                }else if(i <[tileData count]-1){
                    ItemTileView *realTile = [[ItemTileView alloc] init];
                    realTile = [tileData objectAtIndex:i];
                    NSLog(@"[IF]Center Tile: %i X: %f",i,realTile.center.x);
                    NSLog(@"[IF]Center Tile: %i Y: %f",i,realTile.center.y);
                    [tileScroll addSubview:realTile];
                }else{
                    ItemTileView *realTile = [[ItemTileView alloc] init];
                    realTile.itemName = @"bookend";
                    realTile.tileImageName = @"tiles_icon@2x";
                    realTile.tilePortrait = [UIImage imageNamed:realTile.tileImageName];
                    realTile.frame = CGRectMake((20+100*i), 5, 80, 80);
                    NSLog(@"Center Tile: %i X: %f",i,realTile.center.x);
                    NSLog(@"Center Tile: %i Y: %f",i,realTile.center.y);
                    realTile.index = i;
                    [tileScroll addSubview:realTile];
                }
        
        ItemTileView *realTile = [[ItemTileView alloc] init];
        realTile = [tileData objectAtIndex:i];
        NSLog(@"[NO IF]Center Tile:%@ %i X:%0.2f Y:%0.2f",realTile.itemName,i,realTile.center.x,realTile.center.y);
        [tileScroll addSubview:realTile];
    }
    
}

- (BOOL)isDisplayingPageForIndex:(NSUInteger)index
{
    BOOL foundPage = NO;
    for (ItemTileView *itemTile in visibleTiles) {
        if (itemTile.index == index) {
            foundPage = YES;
            break;
        }
    }
    return foundPage;
}

- (ItemTileView *)dequeueRecycledTile
{
    ItemTileView *tile = [recycledTiles anyObject];
    if (tile) {
        [recycledTiles removeObject:tile];
    }
    return tile;
}

- (CGSize)contentSizeForPagingScrollView {
    CGRect bounds = tileScroll.bounds;
    NSLog(@"Bounds: %f   %f ", bounds.size.width, bounds.size.height);
    
    CGSize contentSize = CGSizeMake(220+100 * tileData.count, bounds.size.height);
    NSLog(@"tileData Count: %i",tileData.count);
    NSLog(@"Content: %f   %f ", contentSize.width, contentSize.height);
    return contentSize;
}

#pragma mark -
#pragma mark  Frame calculations
#define PADDING  10

- (CGRect)frameForPagingScrollView {
    int numberOfTiles = tileData.count;
    CGRect frame = CGRectMake(0, 290,20+100*numberOfTiles, 100);
    NSLog(@"frameforPagingScrollView Width: %f",frame.size.width);
    return frame;
}


#pragma Mark Scroll Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //NSLog(@"Scrollview postion: %f",scrollView.contentOffset.x);
    NSArray *tileArray = scrollView.subviews;
    float currentOffset = scrollView.contentOffset.x;
    
    for(int i=1;i < [tileArray count]-1; i++){
        ItemTileView *t = [tileArray objectAtIndex:i];
        float xPoint = t.center.x-160;
        if(( currentOffset > xPoint) && ( currentOffset < xPoint+50))
        {
            itemTitle.text = t.itemName;
            itemImageView.image = t.tilePortrait;
            itemPrice.text = [NSString stringWithFormat: @"%0.2f",[t.price floatValue]];
            reSnags.text = [NSString stringWithFormat: @"%i",[t.reSnags intValue]];
            rateUp.text = [NSString stringWithFormat: @"%i",[t.rateUp intValue]];
            rateDown.text = [NSString stringWithFormat: @"%i",[t.rateDown intValue]];
            snags.text = [NSString stringWithFormat: @"%i",[t.snags intValue]];
        }
    }
}


#pragma Mark Touch Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"Touched");
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:tileScroll];
    
    NSLog(@"Location: %f",location.x);
    
}




#pragma mark -
#pragma mark Tile wrangling


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;// [myProducts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    static NSUInteger const kProductImageTag = 1;
    //    static NSUInteger const kProductNameTag = 2;
    //    static NSUInteger const kShortDescriptionTag = 3;
    //    static NSUInteger const kStoreNameTag = 4;
    //    static NSUInteger const kListNameTag = 5;
    static NSString *kCommentsListCellID = @"CommentsListCellID";
    //
    //    UIImageView *productImage;
    //    UILabel *productName = nil;
    //    UILabel *shortDescription = nil;
    //    UILabel *storeName = nil;
    //    UILabel *listName = nil;
    
    
    //UIFont *specialFont = [UIFont fontWithName:@"Arial-ItalicMT" size:11];
    
  	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCommentsListCellID];
	if (cell == nil) {
        // No reusable cell was available, so we create a new cell and configure its subviews.
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:kCommentsListCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //        productImage = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 63.5, 63.5)] autorelease];
        //        productName.tag = kProductImageTag;
        //        productImage.contentMode = UIViewContentModeScaleAspectFit;
        //        [cell.contentView addSubview:productImage];
        //
        //        productName = [[[UILabel alloc] initWithFrame:CGRectMake(68, 5, 200, 14)] autorelease];
        //        productName.tag = kProductNameTag;
        //        productName.backgroundColor = [UIColor clearColor];
        //        productName.textColor = [UIColor whiteColor];
        //        productName.font = [UIFont boldSystemFontOfSize:14];
        //        [cell.contentView addSubview:productName];
        //
        //        shortDescription = [[[UILabel alloc] initWithFrame:CGRectMake(68, 20, 65, 12)] autorelease];
        //        shortDescription.tag = kShortDescriptionTag;
        //        shortDescription.backgroundColor = [UIColor clearColor];
        //        shortDescription.textColor = [UIColor whiteColor];
        //        shortDescription.font = [UIFont systemFontOfSize:12];
        //        [cell.contentView addSubview:shortDescription];
        //
        //        storeName = [[[UILabel alloc] initWithFrame:CGRectMake(68, 46, 260, 11)] autorelease];
        //        storeName.tag = kStoreNameTag;
        //        storeName.backgroundColor = [UIColor clearColor];
        //        storeName.textColor = [UIColor whiteColor];
        //        storeName.font = [UIFont systemFontOfSize:11];
        //        [cell.contentView addSubview:storeName];
        //
        //
        //        listName = [[[UILabel alloc] initWithFrame:CGRectMake(210, 28, 80, 11)] autorelease];
        //        listName.tag = kListNameTag;
        //        listName.backgroundColor = [UIColor clearColor];
        //        listName.textColor = [UIColor whiteColor];
        //        listName.font = specialFont;
        //        [cell.contentView addSubview:listName];
        
    }else{
        
        //        productImage = (UIImageView *)[cell.contentView viewWithTag:kProductImageTag];
        //        productName = (UILabel *)[cell.contentView viewWithTag:kProductNameTag];
        //        shortDescription = (UILabel *)[cell.contentView viewWithTag:kShortDescriptionTag];
        //        storeName = (UILabel *)[cell.contentView viewWithTag:kStoreNameTag];
        //        listName = (UILabel *)[cell.contentView viewWithTag:kListNameTag];
    }
    
    //    productImage.image = [[UIImage alloc] initWithData:[[myProducts objectAtIndex:indexPath.row] valueForKey:@"imageData"]];
    //    productName.text = [[myProducts objectAtIndex:indexPath.row] valueForKey:@"name"];
    //    shortDescription.text = [[myProducts objectAtIndex:indexPath.row] valueForKey:@"shortDescription"];
    //    // storeName.text = [NSString stringWithFormat:@"from %@",[[myProducts objectAtIndex:indexPath.row] valueForKey:@"storeName"]];
    //    listName.text = [[myProducts objectAtIndex:indexPath.row] valueForKey:@"list"];
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    ItemDetailViewController *idv = [[ItemDetailViewController alloc] initWithNibName:@"ItemDetailViewController" bundle:nil];
    //    idv.name = [[myProducts objectAtIndex:indexPath.row] valueForKey:@"name"];
    //    idv.imageData = [[myProducts objectAtIndex:indexPath.row] valueForKey:@"imageData"]; 
    //    idv.longDescription =  [[myProducts objectAtIndex:indexPath.row] valueForKey:@"longDescription"]; 
    //    [self.navigationController pushViewController:idv animated:YES];
    //    [idv release];
    
}




- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
