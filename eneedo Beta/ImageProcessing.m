//
//  ImageProcessing.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/26/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "ImageProcessing.h"

@implementation ImageProcessing
+(UIImage *)checkEneedoImageCriteria:(NSString *)imageURLString{

    UIImage *productImage;

    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURLString]];
    UIImage *testImage = [UIImage imageWithData:imageData];
    float ht,wd,ratio;
    
    ht = testImage.size.height;
    wd = testImage.size.width;
    ratio = ht/wd;

    if((imageData.length > 2400)&&(ht > 90.)&&(wd > 90.)&&(ratio>0.5)&&(ratio<2.)){
        NSLog(@"Link:%@ Data Len:%i Ht: %f Wd: %f ",imageURLString,imageData.length, testImage.size.height, testImage.size.width);

        productImage = testImage;
    }

return productImage;
}


+(UIImage *)packageSnagImageCrop:(UIImage *)rawSnagImage{

    UIImage *processedImage;
    UIImage *reSizedImage;
    
    CGSize newSizeThumb = CGSizeMake(120,120);

    float hFactor = rawSnagImage.size.height;
    float wFactor = rawSnagImage.size.width;
    float imageRatio = hFactor/wFactor;
    
    
    NSLog(@"image Ratio:%f0.0",imageRatio);
    
    if ((imageRatio >= 0.9)&&(imageRatio <= 1.1)) {
        
    ///Squarish image to be returned
        
        //create a context to do our clipping in
        UIGraphicsBeginImageContext(newSizeThumb);
        [rawSnagImage drawInRect:CGRectMake(0,0,newSizeThumb.width,newSizeThumb.height)];
        processedImage = UIGraphicsGetImageFromCurrentImageContext();
        NSLog(@"Square Image Processed Image: %f0.0 %f0.0",processedImage.size.height, processedImage.size.width);
        
        UIGraphicsEndImageContext();
        ///////////////The temproary test to see the image////////

        //get the documents directory:
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSLog(@"%@",documentsDirectory);
        
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [NSString stringWithFormat:@"%@/testImage.jpg",
                              documentsDirectory];
        
        NSData *imageData = UIImageJPEGRepresentation(processedImage, 1.0);
        
        //save content to the documents directory
        
        [imageData writeToFile:fileName atomically:YES];
        ///////////////The temproary test to see the image////////

    }else if ((imageRatio < 0.9)||(imageRatio > 1.1)){
        
    ///Square image to be returned after cropping
        float newHeight;
        float newWidth;
        float scaleFactor;
        if(hFactor > wFactor){
            newWidth = wFactor;
            newHeight = wFactor;
            
            scaleFactor = wFactor/120;
        
            CGSize newSizeThumb = CGSizeMake(newWidth, newHeight);
            CGSize reSizeThumb = CGSizeMake(newWidth/scaleFactor, newHeight/scaleFactor);

            NSLog(@"Height Image Raw Image: %f0.0 %f0.0",rawSnagImage.size.height, rawSnagImage.size.width);
            UIGraphicsBeginImageContext(newSizeThumb);
            [rawSnagImage drawInRect:CGRectMake(0,-newHeight/4,rawSnagImage.size.width,rawSnagImage.size.height)];
            processedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            UIGraphicsBeginImageContext(reSizeThumb);
            [processedImage drawInRect:CGRectMake(0,0,reSizeThumb.width,reSizeThumb.height)];
            reSizedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            processedImage = reSizedImage;
            NSLog(@"Height Image Processed Image: %f0.0 %f0.0",processedImage.size.height, processedImage.size.width);

            
            ///////////////The temproary test to see the image////////
           //get the documents directory:
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSLog(@"%@",documentsDirectory);
            //make a file name to write the data to using the documents directory:
            NSString *fileName = [NSString stringWithFormat:@"%@/testImage.jpg",documentsDirectory];
            NSData *imageData = UIImageJPEGRepresentation(processedImage, 1.0);
            //save content to the documents directory
            [imageData writeToFile:fileName atomically:YES];
            ///////////////The temproary test to see the image////////

            
        }else{
            newWidth = hFactor;
            newHeight = hFactor;
            scaleFactor = hFactor/120;//
            
            CGSize newSizeThumb = CGSizeMake(newWidth, newHeight);
            CGSize reSizeThumb = CGSizeMake(newWidth/scaleFactor, newHeight/scaleFactor);//
            
            NSLog(@"Wide Image Raw Image: %f0.0 %f0.0",rawSnagImage.size.height, rawSnagImage.size.width);
            UIGraphicsBeginImageContext(newSizeThumb);
            [rawSnagImage drawInRect:CGRectMake(-newWidth/4,0,rawSnagImage.size.width,rawSnagImage.size.height)];
            processedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            UIGraphicsBeginImageContext(reSizeThumb);//
            [processedImage drawInRect:CGRectMake(0,0,reSizeThumb.width,reSizeThumb.height)];//
            reSizedImage = UIGraphicsGetImageFromCurrentImageContext();//
            UIGraphicsEndImageContext();//
            processedImage = reSizedImage;//
            NSLog(@"Wide Image Processed Image: %f0.0 %f0.0",processedImage.size.height, processedImage.size.width);
            
            
        
            ///////////////The temproary test to see the image////////
            //get the documents directory:
            NSArray *paths = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSLog(@"%@",documentsDirectory);
            //make a file name to write the data to using the documents directory:
            NSString *fileName = [NSString stringWithFormat:@"%@/testImage.jpg",documentsDirectory];
            NSData *imageData = UIImageJPEGRepresentation(processedImage, 1.0);
            //save content to the documents directory
            [imageData writeToFile:fileName atomically:YES];
            ///////////////The temproary test to see the image////////
       }
     
    }   
    return processedImage;
}



@end
