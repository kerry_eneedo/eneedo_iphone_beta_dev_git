//
//  ScanViewController.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import <CoreLocation/CoreLocation.h>
#import "Product.h"
#import <FacebookSDK/FacebookSDK.h>

@interface ScanViewController : UIViewController<ZBarReaderDelegate,UIActionSheetDelegate,UIAlertViewDelegate,CLLocationManagerDelegate,UIWebViewDelegate>{
    
    
    ZBarReaderViewController *reader;
    UILabel *_UPCCode;
    UILabel *productName;
    Product *scannedProduct;
    CLLocationManager *locationManager;
    
    
    NSMutableArray *myMobileProducts;
    UILabel *retailLocationLabel;
    UILabel *cityLabel;
    
    BOOL    isScannerOn;
    UIActivityIndicatorView *loadingIndicator;
    id <ZBarReaderViewDelegate> readerDelegate;
    
    
    UIToolbar *browseBar;
    UIWebView *webView;
    
    
    }

@property (strong, nonatomic) ZBarReaderViewController *reader;
@property (strong, nonatomic) UILabel *_UPCCode;
@property (strong, nonatomic) UILabel *productName;
@property (strong, nonatomic) UIActivityIndicatorView *loadingIndicator;
@property (retain, nonatomic) Product *scannedProduct;
@property (retain, nonatomic) UILabel *retailLocationLabel;
@property (retain, nonatomic) UILabel *cityLabel;
@property (retain, nonatomic) NSMutableArray *myMobileProducts;
@property (retain, nonatomic) id<ZBarReaderViewDelegate> readerDelegate;
@property (retain, nonatomic) UIToolbar *browseBar;
@property (retain, nonatomic) UIWebView *webView;

-(void)launchScanner;
-(void)addedProduct:(id)sender;
-(void)startLocationManager;


@end
