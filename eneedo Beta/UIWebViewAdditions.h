//
//  UIWebViewAdditions.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/27/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIWebView(UIWebViewAdditions)
- (CGSize)windowSize;
- (CGPoint)scrollOffset;
@end
