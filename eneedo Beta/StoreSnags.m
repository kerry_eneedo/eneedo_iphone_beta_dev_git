//
//  StoreSnags.m
//  eneedo Beta
//
//  Created by Grunt - Kerry on 1/2/13.
//  Copyright (c) 2013 eneedo. All rights reserved.
//

#import "StoreSnags.h"
#import "AppDelegate.h"

@implementation StoreSnags

+(void)fullStoreSnag: (NSData *)imageData: (NSString *)itemName: (NSString *)imageUrl: (NSNumber *)price: (NSString *)shortDescription: (NSString *)longDescription: (NSNumber *)snags: (NSNumber *)reSnags: (NSNumber *)rateUp: (NSNumber *)rateDown: (NSNumber *)eneedoItemID: (NSString *)storeName: (NSString *)storeLocation{

    
    NSMutableArray *localArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *localDictionary = [[NSMutableDictionary alloc] init];
    
    AppDelegate *appDelegate = [AppDelegate sharedAppDelegate];
    localArray = appDelegate.mobileSnagsInformation;
    
    [localDictionary setValue:imageData forKey:@"imageData"];
    [localDictionary setValue:itemName forKey:@"itemName"];
    [localArray addObject:localDictionary];
    




}

+(void)shortStoreSnag:(NSData *)imageData :(NSString *)itemName :(NSURL *)image_url{
    
    
    NSMutableArray *localArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *localDictionary = [[NSMutableDictionary alloc] init];
    
    AppDelegate *appDelegate = [AppDelegate sharedAppDelegate];
    localArray = appDelegate.mobileSnagsInformation;
    
    [localDictionary setValue:imageData forKey:@"imageData"];
    [localDictionary setValue:itemName forKey:@"itemName"];
    [localDictionary setValue:image_url forKey:@"image_url"];
    [localArray addObject:localDictionary];
    
    appDelegate.mobileSnagsInformation = localArray;
    
    
    
    
        
}


@end
