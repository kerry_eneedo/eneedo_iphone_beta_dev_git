//
//  WebSnagViewController.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/9/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebSnagViewController : UIViewController<UIAlertViewDelegate,UIWebViewDelegate,UITextFieldDelegate> {

    UIWebView           *mainWebView;
    UIImageView         *snagImageView;
    UILabel             *pageURLTitle;
    UITextField         *searchTextField;
    NSString            *rootDomainString;
    NSString            *itemGenericTitle;
    NSURL               *_urlToLoad;
    NSURL               *_urlToHandle;
    
    
    UIActionSheet *_longPressActionSheet;

    
    // Buttons Indexes for UIActionSheet (long tap)
    NSInteger snagButtonIndex;

    NSMutableURLRequest* req;
    
    UIActivityIndicatorView *loadingIndicator;


}
@property (nonatomic,retain) UIWebView      *mainWebView;
@property (nonatomic,retain) UIImageView    *snagImageView;
@property (nonatomic,retain) UILabel        *pageURLTitle;
@property (nonatomic,retain) UITextField    *searchTextField;
@property (nonatomic,retain) NSString       *rootDomainString;
@property (nonatomic,retain) NSString       *itemGenericTitle;
@property (nonatomic, retain, setter=loadURL:) NSURL *url;
@property (nonatomic, retain) UIActionSheet *actionActionSheet;
@property (nonatomic, retain) UIActivityIndicatorView *loadingIndicator;


@end
