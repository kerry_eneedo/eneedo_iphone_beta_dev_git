//
//  UnpreventableUILongPressGestureRecognizer.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/27/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "UnpreventableUILongPressGestureRecognizer.h"

@implementation UnpreventableUILongPressGestureRecognizer

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer {
    return NO;
}

@end