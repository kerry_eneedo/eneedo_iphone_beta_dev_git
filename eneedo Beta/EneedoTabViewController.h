//
//  EneedoTabViewController.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/31/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface EneedoTabViewController : UITabBarController{

    FBSession *session;

}
@property (strong, nonatomic) FBSession *session;

- (void)startLocationManager;

@end
