//
//  LinkCompare.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "LinkCompare.h"

@implementation LinkCompare

+(NSString *)getRootDomain:(NSString *)domain{

    // Return nil if none found.
    NSString * rootDomain = nil;
    
    
    NSArray *hostComponents = [domain componentsSeparatedByString:@"."];
    NSLog(@"Component 1: %@",[hostComponents objectAtIndex:0]);
    NSLog(@"Component 2: %@",[hostComponents objectAtIndex:1]);
    NSLog(@"Component 3: %@",[hostComponents objectAtIndex:2]);
    
    if(hostComponents.count == 3 ){
        NSArray *suffixComponents = [[hostComponents objectAtIndex:2] componentsSeparatedByString:@"/"];
        NSLog(@"Suffix 3: %@",[suffixComponents objectAtIndex:0]);
        rootDomain = [NSString stringWithFormat:@"%@.%@.%@", [hostComponents objectAtIndex:0],[hostComponents objectAtIndex:1],[suffixComponents objectAtIndex:0]];
        NSLog(@" (3 Components) rootDomain: %@",rootDomain);
    }else if(hostComponents.count == 4 ){
        
        NSArray *suffixComponents = [[hostComponents objectAtIndex:3] componentsSeparatedByString:@"/"];
        int suffixLength = [[suffixComponents objectAtIndex:0] length];
        if( suffixLength== 2){
            NSLog(@"Component 4: %@",[hostComponents objectAtIndex:3]);
            NSLog(@"Suffix 4: %@",[suffixComponents objectAtIndex:0]);
            rootDomain = [NSString stringWithFormat:@"%@.%@.%@.%@", [hostComponents objectAtIndex:0],[hostComponents objectAtIndex:1],[hostComponents objectAtIndex:2],[suffixComponents objectAtIndex:0]];
             NSLog(@"(4 Components)  rootDomain: %@",rootDomain);
        }else{
            NSString *shortheaderString = [[hostComponents objectAtIndex:2] substringToIndex:3];

            rootDomain = [NSString stringWithFormat:@"%@.%@.%@", [hostComponents objectAtIndex:0],[hostComponents objectAtIndex:1],shortheaderString];
             NSLog(@"(X Components) rootDomain: %@",rootDomain);
        }
    }
    
    
    return rootDomain;


}
@end
