//
//  ItemsViewController.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemsViewController : UIViewController<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate>{
    
    
    UIView *itemCanvas;
    
    UILabel *itemTitle;
    UILabel *itemPrice;
    UILabel *snags;
    UILabel *reSnags;
    UILabel *rateUp;
    UILabel *rateDown;
    
    UILabel *fakeTileCounterNumber;///Showing tiles
    UIImageView *itemImageView;
    
    UITableView *commentsTable;
    
    UIScrollView *tileScroll;
    
    // Step 2: prepare to tile content
    NSMutableSet *recycledTiles;
    NSMutableSet *visibleTiles;
    
    NSMutableArray *tileData;
    // NSUInteger *tileCounter;
    
}
@property(nonatomic,retain) UIView *itemCanvas;
@property(nonatomic,retain) UILabel *itemTitle;
@property(nonatomic,retain) UILabel *itemPrice;
@property(nonatomic,retain) UILabel *snags;
@property(nonatomic,retain) UILabel *reSnags;
@property(nonatomic,retain) UILabel *rateUp;
@property(nonatomic,retain) UILabel *rateDown;
@property(nonatomic,retain) UILabel *fakeTileCounterNumber;///Showing tiles
@property(nonatomic,retain) UIImageView *itemImageView;
@property(nonatomic,retain) UIScrollView *tileScroll;
@property(nonatomic,retain) UITableView *commentsTable;

@property(nonatomic,retain) NSMutableArray *tileData;


-(CGRect)frameForPagingScrollView;
-(void)constructWheel;
-(CGSize)contentSizeForPagingScrollView;
@end