//
//  Product.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
{
    NSData          *imageData;
    NSString        *name;
    NSNumber        *price;
    NSString        *shortDescription;
    NSString        *longDescription;
    NSString        *list;
    NSNumber        *storeId;
    NSString        *storeName;
    NSString        *storeLocation;
    NSString        *imageUrl;
    
    
}
@property (nonatomic, copy) NSData          *imageData;
@property (nonatomic, copy) NSString        *name;
@property (nonatomic, copy) NSNumber        *price;
@property (nonatomic, copy) NSString        *shortDescription;
@property (nonatomic, copy) NSString        *longDescription;
@property (nonatomic, copy) NSString        *list;
@property (nonatomic, copy) NSNumber        *storeId;
@property (nonatomic, copy) NSString        *storeName;
@property (nonatomic, copy) NSString        *storeLocation;
@property (nonatomic, copy) NSString        *imageUrl;
@end
