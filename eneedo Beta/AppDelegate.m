//  AppDelegate.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.

#import "AppDelegate.h"
#import "ScanViewController.h"
#import "ItemsViewController.h"
#import "ItemTileView.h"
#import "FBLoginViewController.h"
#import "EneedoTabViewController.h"
#import <FacebookSDK/FacebookSDK.h>




static NSString *kAppId = @"420690644634985";
NSString *const ENEEDOSessionStateChangedNotification = @"com.eneedollc.eneed-BETA:ENEEDOSessionStateChangedNotification";
NSString *const SnagTypeChangedNotification = @"test";

@interface AppDelegate ()

@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) EneedoTabViewController *tabViewController;
@property (strong, nonatomic) FBLoginViewController *loginViewController;

- (void)showLoginView;

@end


@implementation AppDelegate
@synthesize window = _window;
@synthesize tabViewController = _tabViewController;

@synthesize navController = _navController;
@synthesize loginViewController = _loginViewController;

@synthesize loadScreen;
@synthesize userInformation;
@synthesize deviceTokenString;
@synthesize startupWheelTitle;
@synthesize mobileSnagsInformation;
@synthesize wheelTitleInformation;
@synthesize logo;
@synthesize session = _session;
@synthesize sessionDict;


#pragma mark -
#pragma mark Facebook Login Code

- (void)createAndPresentLoginView {
    if (self.loginViewController == nil) {
        self.loginViewController = [[FBLoginViewController alloc] init];
        UIViewController *topViewController = [self.navController topViewController];
        [topViewController presentViewController:self.loginViewController animated:NO completion:nil];
    }
}

- (void)showLoginView {
    if (self.loginViewController == nil) {
        [self createAndPresentLoginView];
    } else {
        [self.loginViewController loginFailed];
    }
}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    NSLog(@"Session Status: FBSessionState %@",[session description]);
    
    sessionDict = (NSMutableDictionary *)session;
    
    NSLog(@"Session Dict: FBSessionState %@",[sessionDict description]);

    
    switch (state) {
        case FBSessionStateOpen: {
            [self.tabViewController startLocationManager];
            
            if (self.loginViewController != nil) {
                UIViewController *topViewController = [self.navController topViewController];
                [topViewController dismissViewControllerAnimated:YES completion:nil];
                self.loginViewController = nil;
            }
            
            FBCacheDescriptor *cacheDescriptor = [FBFriendPickerViewController cacheDescriptor];
            [cacheDescriptor prefetchAndCacheForSession:session];
        }
            break;
        case FBSessionStateClosed: {
            NSLog(@"Session Closed: FBSessionState %@",[session description]);

            UIViewController *topViewController = [self.navController topViewController];
            UIViewController *presentViewController = [topViewController presentedViewController];
            if (presentViewController != nil) {
                [topViewController dismissViewControllerAnimated:YES completion:nil];
            }
            [self.navController popToRootViewControllerAnimated:NO];
            [FBSession.activeSession closeAndClearTokenInformation];
            
            [self performSelector:@selector(showLoginView)
                       withObject:nil
                       afterDelay:0.5f];
        }
            break;
        case FBSessionStateClosedLoginFailed: {
            [self performSelector:@selector(showLoginView)
                       withObject:nil
                       afterDelay:0.5f];
        }
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ENEEDOSessionStateChangedNotification
                                                        object:session];
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error: %@",[AppDelegate FBErrorCodeDescription:error.code]]
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSLog(@"allowLoginUI :%i",allowLoginUI);

    return [FBSession openActiveSessionWithReadPermissions:nil
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                             [self sessionStateChanged:session state:state error:error];
                                         }];
}


+ (AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *) [UIApplication sharedApplication].delegate;
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // FBSample logic
    // We need to handle URLs by passing them to FBSession in order for SSO authentication
    // to work.
    NSLog(@"FBSession URL:%@",[url description]);
    NSLog(@"FBSession URL:%@",[url description]);
    return [FBSession.activeSession handleOpenURL:url];
}


- (NSURL *)smartURLForString:(NSString *)str
{
    NSURL *     result;
    NSString *  trimmedStr;
    NSRange     schemeMarkerRange;
    NSString *  scheme;
    
    assert(str != nil);
    
    result = nil;
    
    trimmedStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ( (trimmedStr != nil) && (trimmedStr.length != 0) ) {
        schemeMarkerRange = [trimmedStr rangeOfString:@"://"];
        
        if (schemeMarkerRange.location == NSNotFound) {
            result = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", trimmedStr]];
        } else {
            scheme = [trimmedStr substringWithRange:NSMakeRange(0, schemeMarkerRange.location)];
            assert(scheme != nil);
            
            if ( ([scheme compare:@"http"  options:NSCaseInsensitiveSearch] == NSOrderedSame)
                || ([scheme compare:@"https" options:NSCaseInsensitiveSearch] == NSOrderedSame) ) {
                result = [NSURL URLWithString:trimmedStr];
            } else {
                NSString *eneedoSearchStr = str;
                NSLog(@"eneedoSttr: %@",eneedoSearchStr);
                NSString *taggedStr = [NSString stringWithFormat:@"eneedoSearch-%@",str];
                result = [NSURL URLWithString:taggedStr];
            }
        }
    }
    
    return result;
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    ///Set Global UINavigation Bars
    [self customizeNavBars];
    
    // BUG WORKAROUND:
    // http://stackoverflow.com/questions/1725881/unknown-class-myclass-in-interface-builder-file-error-at-runtime
    [FBProfilePictureView class];
    
    //Read Wheels List Information
    NSError *errorWheelTitleList;
    NSArray *pathsWheelTitleList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryWheelTitleList = [pathsWheelTitleList objectAtIndex:0];
    NSString *pathWheelTitleList = [documentsDirectoryWheelTitleList stringByAppendingPathComponent:@"WheelTitleList.plist"];
    NSFileManager *fileManagerWheelTitleList = [NSFileManager defaultManager];
    if (![fileManagerWheelTitleList fileExistsAtPath: pathWheelTitleList])
    {
        NSString *bundleWheelTitleList = [[NSBundle mainBundle] pathForResource:@"WheelTitleList" ofType:@"plist"];
        [fileManagerWheelTitleList copyItemAtPath:bundleWheelTitleList toPath: pathWheelTitleList error:&errorWheelTitleList];
    }
    
    wheelTitleInformation = [[NSMutableArray alloc] initWithContentsOfFile:pathWheelTitleList];
    
    for (int i = 0; i < [wheelTitleInformation count]; i++)
    {
        NSLog(@"Wheel Title (%i): %@",i,[[wheelTitleInformation objectAtIndex:i]valueForKey:@"wheelName"]);
        NSLog(@"Wheel Title (%i): %@",i,[[wheelTitleInformation objectAtIndex:i]valueForKey:@"wheelID"]);
        NSLog(@"Wheel Title (%i): %i",i,[[[wheelTitleInformation objectAtIndex:i]objectForKey:@"visible"] intValue]);
       
        
        //////////////////////////This selects which wheel to start from//////////////////////////////////////////
        
        if([[[wheelTitleInformation objectAtIndex:i]objectForKey:@"visible"] intValue] == 1)
        {
            startupWheelTitle = [[wheelTitleInformation objectAtIndex:i]valueForKey:@"wheelID"];
            NSLog(@"startup Title: %@",startupWheelTitle);
        }
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *pathForFile = [NSString stringWithFormat:@"%@.plist",startupWheelTitle];
    
    if ([fileManager fileExistsAtPath:pathForFile])
    {//NSLog(@"file: %@",pathForFile);
        //Read Mobile Snags Information
        NSError *errorMobileSnags;
        NSArray *pathsMobileSnags = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectoryMobileSnags = [pathsMobileSnags objectAtIndex:0];
        NSString *pListTitle = [NSString stringWithFormat:@"%@.plist",startupWheelTitle];
        NSString *pathMobileSnags = [documentsDirectoryMobileSnags stringByAppendingPathComponent:pListTitle];
        NSFileManager *fileManagerMobileSnags = [NSFileManager defaultManager];
        if (![fileManagerMobileSnags fileExistsAtPath: pathMobileSnags])
        {
            NSString *bundleMobileSnags = [[NSBundle mainBundle] pathForResource:startupWheelTitle ofType:@"plist"];
            [fileManagerMobileSnags copyItemAtPath:bundleMobileSnags toPath: pathMobileSnags error:&errorMobileSnags];
        }
        mobileSnagsInformation = [[NSMutableArray alloc] initWithContentsOfFile:pathMobileSnags];
        
        for (int i = 0; i < [mobileSnagsInformation count]; i++)
        {
            ItemTileView *itemContainer = [[ItemTileView alloc] init];
            itemContainer.shortDescription = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"itemDescription"];
            itemContainer.itemName = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"itemName"];
            itemContainer.price = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"price"];
            itemContainer.snags = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"snags"];
            itemContainer.reSnags = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"reSnags"];
            itemContainer.rateUp = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"rateUp"];
            itemContainer.rateDown = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"rateDown"];
            itemContainer.tileImageName = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"tileImageName"];
            itemContainer.imageData = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"imageData"];
            UIImage *img = [[UIImage alloc] initWithData:itemContainer.imageData];
            itemContainer.tilePortrait = img;
            itemContainer.frame = CGRectMake((20+100*i), 5, 80, 80);
            itemContainer.index = i;
            
            [mobileSnagsInformation removeObjectAtIndex:i ];
            [mobileSnagsInformation insertObject:itemContainer atIndex:i];
            NSLog(@" [IF] App Delegate  item Name: %@ item URL: %@ item Description: %@ item Image Dims: %f0.2 %f0.2",itemContainer.itemName,itemContainer.image_url,itemContainer.shortDescription,itemContainer.tilePortrait.size.width,itemContainer.tilePortrait.size.height);
            
        }
        /////// Make a Bookend Tiles and Insert into Wheel
        
        ItemTileView *frontBookend = [[ItemTileView alloc] init];
        frontBookend.itemName = @"bookend";
        frontBookend.image_url = [NSURL URLWithString:@"http://eneedo.grunt-software.com/images/tiles_icon2x.jpg"];
        frontBookend.tilePortrait = [UIImage imageNamed:@"tiles_icon@2x.jpg"];
        frontBookend.frame = CGRectMake(120, 5, 80, 80);
        [mobileSnagsInformation insertObject:frontBookend atIndex:0];
        
        ItemTileView *backBookend = [[ItemTileView alloc] init];
        backBookend.itemName = @"bookend";
        backBookend.image_url = [NSURL URLWithString:@"http://eneedo.grunt-software.com/images/tiles_icon2x.jpg"];
        backBookend.tilePortrait = [UIImage imageNamed:@"tiles_icon@2x.jpg"];
        backBookend.frame = CGRectMake((20+100*[mobileSnagsInformation count]), 5, 80, 80);
        [mobileSnagsInformation insertObject:backBookend atIndex:[mobileSnagsInformation count]];
        
        /////// Make a Bookend Tiles and Insert into Wheel
        
        
    }else{
        NSLog(@"Downloading Wheel Data....");
        [self queryWheelData:startupWheelTitle];
        
        for (int i = 0; i < [mobileSnagsInformation count]; i++){
            ItemTileView *itemContainer = [[ItemTileView alloc] init];
            itemContainer.shortDescription = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"shortDescription"];
            itemContainer.itemName = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"itemName"];
            itemContainer.image_url = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"image_url"];
            id path = itemContainer.image_url;
            NSURL *url = [NSURL URLWithString:path];
            NSError *err;
            NSData *data = [NSData dataWithContentsOfURL:url options:NSDataReadingMappedIfSafe error:&err];
            UIImage *img = [[UIImage alloc] initWithData:data];
            itemContainer.tilePortrait = img;
            itemContainer.price = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"price"];
            itemContainer.snags = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"snags"];
            itemContainer.reSnags = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"reSnags"];
            itemContainer.rateUp = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"rateUp"];
            itemContainer.rateDown = [[mobileSnagsInformation objectAtIndex:i] valueForKey:@"rateDown"];
            itemContainer.frame = CGRectMake((20+100*i), 5, 80, 80);
            itemContainer.index = i;
            
            [mobileSnagsInformation removeObjectAtIndex:i ];
            [mobileSnagsInformation insertObject:itemContainer atIndex:i];
            NSLog(@" [ELSE] AppDel itemName:%@ itemURL:%@ itemDescription:%@ itemImage Dims: %f0.2 %f0.2",itemContainer.itemName,itemContainer.image_url,itemContainer.shortDescription,itemContainer.tilePortrait.size.width,itemContainer.tilePortrait.size.height);

            
        }
        
        /////// Make a Bookend Tiles and Insert into Wheel
        
        ItemTileView *frontBookend = [[ItemTileView alloc] init];
        frontBookend.itemName = @"bookend";
        frontBookend.image_url = [NSURL URLWithString:@"http://eneedo.grunt-software.com/images/tiles_icon2x.jpg"];
        NSError *err;
        NSData *data = [NSData dataWithContentsOfURL:frontBookend.image_url options:NSDataReadingMappedIfSafe error:&err];
        UIImage *img = [[UIImage alloc] initWithData:data];
        frontBookend.tilePortrait = img;
        frontBookend.frame = CGRectMake(20, 5, 80, 80);
        [mobileSnagsInformation insertObject:frontBookend atIndex:0];
        
        ItemTileView *backBookend = [[ItemTileView alloc] init];
        backBookend.itemName = @"bookend";
        backBookend.image_url = [NSURL URLWithString:@"http://eneedo.grunt-software.com/images/tiles_icon2x.jpg"];
        NSError *err2;
        NSData *data2 = [NSData dataWithContentsOfURL:backBookend.image_url options:NSDataReadingMappedIfSafe error:&err2];
        UIImage *img2 = [[UIImage alloc] initWithData:data2];
        backBookend.tilePortrait = img2;
        backBookend.frame = CGRectMake((20+100*[mobileSnagsInformation count]), 5, 80, 80);
        [mobileSnagsInformation insertObject:backBookend atIndex:[mobileSnagsInformation count]];
       
        /////// Make a Bookend Tiles and Insert into Wheel
        
    }
    //Read User Information
    NSError *errorUserInfo;
    NSArray *pathsUserInfo = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryUserInfo = [pathsUserInfo objectAtIndex:0];
    NSString *pathUserInfo = [documentsDirectoryUserInfo stringByAppendingPathComponent:@"UserInformation.plist"];
    NSFileManager *fileManagerUserInfo = [NSFileManager defaultManager];
    if (![fileManagerUserInfo fileExistsAtPath: pathUserInfo])
    {
        NSString *bundleUserInfo = [[NSBundle mainBundle] pathForResource:@"UserInformation" ofType:@"plist"];
        [fileManagerUserInfo copyItemAtPath:bundleUserInfo toPath: pathUserInfo error:&errorUserInfo];
    }
    userInformation = [[NSMutableDictionary alloc] initWithContentsOfFile:pathUserInfo];
    
    
    NSLog(@"User info: %@",[userInformation description]);
    // Set the application defaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObject:@"YES" forKey:@"autoLoginStatus"];
    [defaults registerDefaults:appDefaults];
    [defaults synchronize];
    
        
    UIViewController *tab1_Scan = [[ScanViewController alloc] init];
    UIViewController *tab2_Items = [[ItemsViewController alloc] init];
    
    UINavigationController *tab1Nav = [[UINavigationController alloc] initWithRootViewController:tab1_Scan];
    tab1Nav.navigationBarHidden = YES;
    UINavigationController *tab2Nav = [[UINavigationController alloc] initWithRootViewController:tab2_Items];
    tab2Nav.navigationBarHidden = YES;
    self.tabViewController = [[EneedoTabViewController alloc] init];
    [self.tabViewController setWantsFullScreenLayout:NO];
    self.tabViewController.viewControllers = [NSArray arrayWithObjects:tab1Nav,tab2Nav,nil];//tab1_Scan,tab2_Items
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.tabViewController];
    self.window.rootViewController = self.navController;
    [self.window makeKeyAndVisible];
    
    // FBSample logic
    // See if we have a valid token for the current state.
    if (![self openSessionWithAllowLoginUI:NO]) {
        // No? Display the login page.
        [self showLoginView];
    }
    
    return YES;
   
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

    
    //Write Information
    NSError *error;
    NSString *errorString;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"MobileSnagsList.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path])
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"MobileSnagsList" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath: path error:&error];
    }
    
    NSMutableArray *rootObj = [NSMutableArray arrayWithCapacity:[mobileSnagsInformation count]];
    NSDictionary *innerDict;
    NSLog(@"Saving Items count: %i", [mobileSnagsInformation count]);
    
    for(int i=0;i<[mobileSnagsInformation count];i++){
        innerDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [[mobileSnagsInformation valueForKey:@"itemName"]objectAtIndex:i],[[mobileSnagsInformation valueForKey:@"shortDescription"] objectAtIndex:i],[[mobileSnagsInformation valueForKey:@"longDescription"] objectAtIndex:i] ,[[mobileSnagsInformation valueForKey:@"image_url"] objectAtIndex:i] ,[[mobileSnagsInformation valueForKey:@"imageData"] objectAtIndex:i],[[mobileSnagsInformation valueForKey:@"snags"] objectAtIndex:i],[[mobileSnagsInformation valueForKey:@"reSnags"] objectAtIndex:i],[[mobileSnagsInformation valueForKey:@"rateUp"] objectAtIndex:i] ,[[mobileSnagsInformation valueForKey:@"rateDown"] objectAtIndex:i],[[mobileSnagsInformation valueForKey:@"price"] objectAtIndex:i],[[mobileSnagsInformation valueForKey:@"eneedoItemID"] objectAtIndex:i],nil] forKeys:[NSArray arrayWithObjects:@"itemName", @"shortDescription",@"longDescription", @"image_url",@"imageData",@"snags",@"reSnags",@"rateUp",@"rateDown",@"price",@"eneedoItemID",nil]];
        
        
        [rootObj insertObject:innerDict atIndex:i];
    }
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:(id)rootObj  format:NSPropertyListXMLFormat_v1_0 errorDescription:&errorString];
    
    if(plistData) {
        NSLog(@"No error creating Plist data.");
        [plistData writeToFile:path atomically:YES];
    }else{
        NSLog(@"Error saving Plist:");
        
        for (int i =0; i < [mobileSnagsInformation count]; i++){
            
           ///// Uncomment if for troubleshooting
            
//            NSLog(@"First Name:%@", [[friends objectAtIndex:i] valueForKey:@"firstName"]);
//            NSLog(@"Last  Name:%@", [[friends objectAtIndex:i] valueForKey:@"lastName"]);
//            NSLog(@"cell Name:%@", [[friends objectAtIndex:i] valueForKey:@"cellNumber"]);
//            NSLog(@"Image:%i", [[[friends objectAtIndex:i] valueForKey:@"herImage"]length]);
//            NSLog(@"birthdate:%@", [[friends objectAtIndex:i] valueForKey:@"birthdate"]);
//            NSLog(@"movie:%@", [[friends objectAtIndex:i] valueForKey:@"favoriteMovieGenre"]);
//            NSLog(@"music:%@", [[friends objectAtIndex:i] valueForKey:@"favoriteMusicGenre"]);
//            NSLog(@"color:%@", [[friends objectAtIndex:i] valueForKey:@"favoriteColor"]);
//            NSLog(@"animal:%@", [[friends objectAtIndex:i] valueForKey:@"favoriteAnimal"]);
//            NSLog(@"Notes:%@", [[friends objectAtIndex:i] valueForKey:@"notes"]);
//            NSLog(@"favoriteBeverage:%@", [[friends objectAtIndex:i] valueForKey:@"favoriteBeverage"]);
//            NSLog(@"chineseZodiac:%@", [[friends objectAtIndex:i] valueForKey:@"chineseZodiac"]);
//            NSLog(@"horoscope:%@", [[friends objectAtIndex:i] valueForKey:@"horoscope"]);
//            NSLog(@"rating:%@", [[friends objectAtIndex:i] valueForKey:@"rating"]);
        }
        NSLog(@"%@",errorString);
    }
    
    
    
    
}






- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

-(void)applicationWillTerminate:(UIApplication *)application {
    // FBSample logic
    // if the app is going away, we close the session object; this is a good idea because
    // things may be hanging off the session, that need releasing (completion block, etc.) and
    // other components in the app may be awaiting close notification in order to do cleanup
    [FBSession.activeSession close];
}

- (void)applicationDidBecomeActive:(UIApplication *)application	{
    // FBSample logic
    // We need to properly handle activation of the application with regards to SSO
    //  (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
    [FBSession.activeSession handleDidBecomeActive];
}

-(void)customizeNavBars{
    // Create resizable images
    UIImage *eneedoGradient = [UIImage imageNamed:@"eneedonavbar_custom"];
    
    // Set the background image for *all* UINavigationBars
    [[UINavigationBar appearance] setBackgroundImage:eneedoGradient
                                       forBarMetrics:UIBarMetricsDefault];
    
}


-(void)queryWheelData:(NSString *)wheelTitle{
    //NSString *query = [NSString stringWithFormat:@"http://eneedo.grunt-software.com/testwheel/%@",wheelTitle];
    // NSString *username = @"kerryuser";
    NSString *query = [NSString stringWithFormat:@"http://eneedo.grunt-software.com/testwheel/index.php?wheelid=%@",wheelTitle];///Grabbing Remote data
    
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:query]];
    NSError *error;
    NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    mobileSnagsInformation = [[NSMutableArray alloc] initWithArray: json];
    NSLog(@"wheel: %@",[mobileSnagsInformation description]);
}

#pragma mark FACEBOOK Methods


+ (NSString *)FBErrorCodeDescription:(FBErrorCode) code {
    switch(code){
        case FBErrorInvalid :{
            return @"FBErrorInvalid";
        }
        case FBErrorOperationCancelled:{
            return @"FBErrorOperationCancelled";
        }
        case FBErrorLoginFailedOrCancelled:{
            return @"FBErrorLoginFailedOrCancelled";
        }
        case FBErrorRequestConnectionApi:{
            return @"FBErrorRequestConnectionApi";
        }case FBErrorProtocolMismatch:{
            return @"FBErrorProtocolMismatch";
        }
        case FBErrorHTTPError:{
            return @"FBErrorHTTPError";
        }
        case FBErrorNonTextMimeTypeReturned:{
            return @"FBErrorNonTextMimeTypeReturned";
        }
        case FBErrorNativeDialog:{
            return @"FBErrorNativeDialog";
        }
        default:
            return @"[Unknown]";
    }
}

- (void) closeSession {
    [FBSession.activeSession closeAndClearTokenInformation];
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
