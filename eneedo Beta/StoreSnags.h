//
//  StoreSnags.h
//  eneedo Beta
//
//  Created by Grunt - Kerry on 1/2/13.
//  Copyright (c) 2013 eneedo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreSnags : NSObject  
+(void)fullStoreSnag: (NSData *)imageData: (NSString *)itemName: (NSNumber *)price: (NSString *)shortDescription: (NSString *)longDescription: (NSNumber *)snags: (NSNumber *)reSnags: (NSNumber *)rateUp: (NSNumber *)rateDown: (NSNumber *)eneedoItemID: (NSString *)storeName: (NSString *)storeLocation: (NSString *)imageUrl;

+(void)shortStoreSnag: (NSData *)imageData: (NSString *)itemName: (NSURL *)image_url;
@end
