//
//  ScanViewController.m
//  eneedo BETA
//
//  Created by Grunt - Kerry on 10/21/12.
//  Copyright (c) 2012 eneedo. All rights reserved.
//

#import "ScanViewController.h"
#import "AppDelegate.h"
#import "WebSnagViewController.h"
#import <FacebookSDK/FacebookSDK.h>


//#import "ItemDetailViewController.h"

@interface ScanViewController () <UIActionSheetDelegate>
@property (strong, nonatomic) FBCacheDescriptor *placeCacheDescriptor;
@property (strong, nonatomic) CLLocationManager *locationManager;



- (void)setPlaceCacheDescriptorForCoordinates:(CLLocationCoordinate2D)coordinates;

@end



@implementation ScanViewController
@synthesize _UPCCode;
@synthesize productName;
@synthesize loadingIndicator;
@synthesize reader;
@synthesize scannedProduct;
@synthesize retailLocationLabel;
@synthesize cityLabel;
@synthesize myMobileProducts;
@synthesize readerDelegate;
@synthesize browseBar;
@synthesize webView;
@synthesize locationManager = _locationManager;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = NSLocalizedString(@"Snag", @"Snag");
        self.tabBarItem.image = [UIImage imageNamed:@"snag_icon"];

    }
    return self;
}


- (void)viewDidLoad{
    [super viewDidLoad];
    [self startLocationManager];
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    self.view = view;
        
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems: [NSArray arrayWithObjects:
                                              @"Scan",
                                              @"Snag",
                                              nil]];
    [segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    segmentedControl.frame = CGRectMake(20, 320, 280, 35);
    segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    segmentedControl.momentary = YES;
    
    [self.view addSubview:segmentedControl];

        
}
- (void)segmentAction:(id)sender {
    
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    if(segmentedControl.selectedSegmentIndex == 0) {
        
        
        
    } else if(segmentedControl.selectedSegmentIndex == 1) {
        
        WebSnagViewController *wsv = [[WebSnagViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:wsv];
        nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:nav animated:YES completion:nil];
        
    }

    }
-(void)startLocationManager{

    ////Location Manager Code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];


}


-(void)launchScanner{
    
    NSLog(@"Inside Launch Scanner");
    
    ZBarImageScanner *scanner = reader.scanner;
    [scanner setSymbology:ZBAR_I25
                   config:ZBAR_CFG_ENABLE
                       to:0];//disable rarely used I2/5 to  improve
    
    isScannerOn = YES;
    // force class to load so it may be referenced directly from nib
    reader = [[ZBarReaderViewController alloc] init];
    if(isScannerOn == YES){
        reader.readerDelegate = self;
    }else{
        reader.readerDelegate = nil;
    }
    reader.showsZBarControls = NO;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    reader.readerView.trackingColor = [UIColor colorWithRed:0.505 green:0.819 blue:0.443 alpha:1.0];
    reader.showsCameraControls = NO;
    reader.wantsFullScreenLayout = NO;
    
    UIView *eneedoFrame = [[UIView alloc] init];
    eneedoFrame.frame = CGRectMake(0, 0, 200, 200);
    eneedoFrame.backgroundColor = [UIColor clearColor];
    
    UIImageView *snagFrameOverlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"snagoverlay7_640x960"]];
    snagFrameOverlay.frame = CGRectMake(0, 0, 320, 480);
    [eneedoFrame addSubview:snagFrameOverlay];
        
    UIButton *snagButton = [UIButton buttonWithType:UIButtonTypeCustom];
    snagButton.frame = CGRectMake(123.5, 281, 75, 75);
    [snagButton setImage:[UIImage imageNamed:@"snagbutton"] forState:UIControlStateNormal];
    [snagButton addTarget:self action:@selector(snagAnItem:) forControlEvents:UIControlEventTouchUpInside];
    [eneedoFrame addSubview:snagButton];

    reader.cameraOverlayView = eneedoFrame;
    
    [self.navigationController addChildViewController:reader];
   

    
    //    loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    //    loadingIndicator.frame = CGRectMake(140, 160, 40, 40);
    //    loadingIndicator.backgroundColor = [UIColor clearColor];
    //    loadingIndicator.color = [UIColor colorWithRed:0.505 green:0.819 blue:0.443 alpha:1.0];
    //    loadingIndicator.hidden = YES;
    //    loadingIndicator.hidesWhenStopped = YES;
    //    [eneedoFrame addSubview:loadingIndicator];
    //    [loadingIndicator startAnimating];
    //    loadingIndicator.hidden = NO;
    
}


// ZBarReaderDelegate

- (void)  imagePickerController: (UIImagePickerController*) picker
  didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    [loadingIndicator startAnimating];
    loadingIndicator.hidden = NO;
    
    isScannerOn = NO;
    
    //    UIImage *image = [info objectForKey: UIImagePickerControllerOriginalImage];
    //    results.resultImage.image = image;
    NSMutableDictionary *newScan = [[NSMutableDictionary alloc] init];
    
    id <NSFastEnumeration> syms =
    [info objectForKey: ZBarReaderControllerResults];
    for(ZBarSymbol *sym in syms) {
        _UPCCode.text = sym.data;
        [newScan setValue:sym.data forKey:@"upc"];
        break;
    }
    
    
    float lat = locationManager.location.coordinate.latitude;
    float lon = locationManager.location.coordinate.longitude;
    float alt = locationManager.location.altitude;
    float hAcc = locationManager.location.horizontalAccuracy;
    float vAcc = locationManager.location.verticalAccuracy;
    
    
    [newScan setValue:[NSNumber numberWithFloat:lat] forKey:@"latitude"];
    [newScan setValue:[NSNumber numberWithFloat:lon] forKey:@"longitude"];
    [newScan setValue:[NSNumber numberWithFloat:alt] forKey:@"altitude"];
    [newScan setValue:[NSNumber numberWithFloat:hAcc] forKey:@"horizontalAccuracy"];
    [newScan setValue:[NSNumber numberWithFloat:vAcc] forKey:@"verticalAccuracy"];
    
    [self performSelector:@selector(turnScannerOn) withObject:nil afterDelay:3.0];
    
    
}

-(void)turnScannerOn{
    
    isScannerOn = YES;
    [loadingIndicator stopAnimating];
}



//-(void)viewWillAppear:(BOOL)animated{
//    
//    [super viewWillAppear:animated];
//    
//    reader.wantsFullScreenLayout = NO;
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//    int stateNum = [[appDelegate.userInformation objectForKey:@"snagWithWebView"] intValue];
//    NSLog(@"snagWithWebView state %i",stateNum );
//
//    switch (stateNum) {
//        case 0:
//            
//            if(browseBar){
//                [browseBar removeFromSuperview];
//            }
//            if(webView){
//                [webView removeFromSuperview];
//            }
//            NSLog(@"Launch Scanner in ViewWillAppear");
//            [self launchScanner];
//            break;
//        case 1:
//            NSLog(@"Put in Borwse Bar");
//            break;
//        default:
//            break;
//    }
//            
////        browseBar  = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
////        [self.view addSubview:browseBar];
////        
////        webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 44, 320, 320)];
////        [self.view addSubview:webView];
//        
//   
//
//
//}

-(void)confirmLocation:(id)sender{
        
    UIActionSheet *resetLocation = [[UIActionSheet alloc] initWithTitle:@"Wrong Location?:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"The Container Store",@"Big 5", nil];
    resetLocation.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [resetLocation showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
}


- (void)setPlaceCacheDescriptorForCoordinates:(CLLocationCoordinate2D)coordinates {
    self.placeCacheDescriptor =
    [FBPlacePickerViewController cacheDescriptorWithLocationCoordinate:coordinates
                                                        radiusInMeters:1000
                                                            searchText:@"restaurant"
                                                          resultsLimit:50
                                                      fieldsForRequest:nil];
}
-(void)addedProduct:(id)sender{
    
    
    if(scannedProduct){
        UIAlertView *added = [[UIAlertView alloc] init];
        
        NSString *snag =[NSString stringWithFormat:@"You added %@ to Mobile Snags!",[scannedProduct valueForKey:@"name"]];
        [added setTitle:NSLocalizedString(snag,"")];
        [added setDelegate:self];
        [added addButtonWithTitle:NSLocalizedString(@"OK","")];
        [added addButtonWithTitle:NSLocalizedString(@"Cancel","")];
        [added show];
        
        productName.text = @"";
        retailLocationLabel.text = @"";
        cityLabel.text = @"";
        _UPCCode.text = @"";
    }else{
        
        UIAlertView *notAdded = [[UIAlertView alloc] init];
        [notAdded setTitle:NSLocalizedString(@"Sorry. The product was not found.","")];
        [notAdded setDelegate:self];
        [notAdded addButtonWithTitle:NSLocalizedString(@"OK","")];
        [notAdded addButtonWithTitle:NSLocalizedString(@"Cancel","")];
        [notAdded show];
        
        productName.text = @"";
        
        retailLocationLabel.text = @"";
        cityLabel.text = @"";
        _UPCCode.text = @"";
    }
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    ////Do something here if the item is kept or rejected
    
    
}


#pragma mark - WebView methods


-(void)webViewDidStartLoad:(UIWebView *)webView{
    //[activityIndicator startAnimating];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    //[activityIndicator stopAnimating];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    //addressBar.text =  @"URL NOT FOUND" ;
    //[activityIndicator stopAnimating];
    
    
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    //addressBar.text = @"";
    //    NSURL *url = [request URL];
    //    NSString *display = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    
    
    
    //    if([[url scheme] isEqualToString:@"http://"]){
    //
    //                [addressBar setText:@"gg"];
    //                [self goToAddress:nil];
    return YES;
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end