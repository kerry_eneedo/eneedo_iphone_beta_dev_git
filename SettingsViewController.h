//
//  SettingsViewController.h
//  eneedo BETA
//
//  Created by Grunt - Kerry on 11/5/12.
//  Copyright (c) 2012 eneedo. All rights reserved.

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController{

    UISwitch *snagSwitch;
    UIButton *cancelButton;

    NSMutableDictionary *userInfoDict;
    
    NSNumber *switchStateNumber;


}
@property (nonatomic,retain)  UISwitch *snagSwitch;
@property (nonatomic,retain)  UIButton *cancelButton;
@property (retain, nonatomic) NSMutableDictionary *userInfoDict;
@property (nonatomic,retain)  NSNumber *switchStateNumber;




@end
